from flask import Flask
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin

from core.resources.db_connections import Connections
from core.resources.login import Login
from core.resources.sqlbus import SQLBus
from core.resources.databases import Databases
from core.resources.objects import Schemas, Tables, Indexes, Views, Functions, Procedures, Triggers
from core.resources.ddlgenerate import DDLGen
from core.resources.getsql import GetSQL
from core.resources.sqlstatement import SQLStatement
app = Flask(__name__)
CORS(app)
app.secret_key = 'oQwek63C'
api = Api(app)
api.add_resource(Connections, '/connect')
api.add_resource(Login, '/login')
api.add_resource(SQLBus, '/runsql')
api.add_resource(Databases, '/databases')
api.add_resource(Schemas, '/schemas')
api.add_resource(Tables, '/tables')
api.add_resource(Indexes, '/indexes')
api.add_resource(Views, '/views')
api.add_resource(Functions, '/functions')
api.add_resource(Procedures, '/procedures')
api.add_resource(Triggers, '/triggers')
api.add_resource(DDLGen, '/getddl')
api.add_resource(GetSQL, '/getsql')
api.add_resource(SQLStatement, '/sqlstatement')

if __name__ == '__main__':
    app.run(debug=True)

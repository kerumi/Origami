import pynuodb, json, ast
import datetime, time
def is_valid_type(type):
    return type == 'create' or type == 'complete' or type == 'alter' or type == 'delete'

def get_create_ddl(creds, object):
    connection = pynuodb.connect(**creds)
    cursor = connection.cursor()
    cursor.execute("")

def stringify_type(data_type):
    if data_type == str:
        return "string"
    elif data_type == int:
        return "bigint"
    elif data_type == chr:
        return "char"
    elif data_type == float:
        return "float"
    elif data_type == complex:
        return "double"

def create_ddl(column):
    for c in column:
        data_type = c[1]
        print data_type == datetime
        # data_type = stringify_type(data_type)

class DDL:
    """NuoDB DDL generator class"""
    def __init__(self, credentials):
        self.credentials = credentials
    #ddl types: complete (from object returns full ddl), create (from object return insert ddl),
    #alter (from object return alter ddl), delete (from object return delete*cascade* ddl)
    #object param is a dict that have type, name
    def generate(self, object, ddl_type):
        if is_valid_type(ddl_type):
            connection = pynuodb.connect(**self.credentials)
            cursor = connection.cursor()
            name = object['name']
            stmt = "SELECT * FROM "+name+";"
            cursor.execute(stmt.encode('utf-8'))
            ddl = create_ddl(cursor.description)

            # for row in cursor.fetchall():
            #     print row
            # ddl_result = {
            #     'create': lambda x: get_create_ddl(creds, object)
            # }[ddl_type](x)
            #
            # connection.commit()

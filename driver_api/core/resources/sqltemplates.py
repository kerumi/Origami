def get_table_insert():
    return r"""CREATE TABLE [ IF NOT EXISTS ] [schema_name.]table_name
( column_name { data_type | domain_name } [ column_constraints ] [ , ... ]
[, table_constraints ]
)"""

def get_table_modify():
    return r"""ALTER TABLE [schema_name.]table_name alter_table_command [ ,alter_table_command ]"""

def get_table_delete():
    return r"""DROP TABLE
    [IF EXISTS] [CASCADE | RESTRICT] [schema.]table_name
DROP TABLE
    [schema.]table_name [IF EXISTS] [CASCADE | RESTRICT]"""

def get_index_insert():
    return r"""CREATE [ UNIQUE ] INDEX index_name ON [schema.]table_name(column_name[, ...] )
    [ WITH ( [RESOLUTION n],[MAX KEY SIZE n] ) ]"""

def get_index_modify():
    return r""""""

def get_index_delete():
    return r"""DROP INDEX [schema.]index_name [IF EXISTS]"""

def get_function_insert():
    return r"""SET DELIMITER @
CREATE FUNCTION FUNCTION1 (p1 STRING)
	 RETURNS STRING
	 DETERMINISTIC
	 LANGUAGE SQL
AS
	RETURN p1;

END_FUNCTION"""

def get_function_modify():
    return r"""ALTER FUNCTION TEST(p1 STRING)
	 RETURNS STRING
	 DETERMINISTIC
	 LANGUAGE SQL
AS
	RETURN p1;

END_FUNCTION"""

def get_function_delete():
    return r"""DROP FUNCTION IF EXISTS [schema.]func_name[/num_arg]"""

def get_procedure_insert():
    return r"""SET DELIMITER @
CREATE PROCEDURE PROCEDURE1 (IN P_IN INTEGER, IN P_OUT INTEGER) AS

	 P_OUT = P_IN;

END_PROCEDURE
@"""

def get_procedure_modify():
    return r"""ALTER PROCEDURE PROCEDURE1(IN P_IN INTEGER, IN P_OUT INTEGER) AS

	 P_OUT = P_IN;

END_PROCEDURE"""

def get_procedure_delete():
    return r"""DROP PROCEDURE [ IF EXISTS ] [schema.]sp_name"""

def get_trigger_insert():
    return r"""SET DELIMITER @
CREATE TRIGGER SINS_audit_trigger FOR SINS AFTER INSERT AS

END_TRIGGER
@"""

def get_trigger_modify():
    return r"""ALTER TRIGGER name FOR name POSITION 0 INACTIVE"""

def get_trigger_delete():
    return r"""DROP TRIGGER [schema.]trigger_name [ IF EXISTS ]"""

def get_view_insert():
    return r"""CREATE VIEW [schema.]view [ ( column_name [ ,column_name ]... ) ] AS SELECT query"""

def get_view_modify():
    return r"""ALTER VIEW [schema.]view [ ( column_name [ ,column_name ]... ) ] AS SELECT query"""

def get_view_delete():
    return r"""DROP VIEW
    [schema.] view_name [IF EXISTS] [CASCADE | RESTRICT]"""

def get_schema_insert():
    return r"""CREATE SCHEMA schema_name"""

def get_schema_modify():
    return r""""""

def get_schema_delete():
    return r"""DROP SCHEMA [ CASCADE | RESTRICT] [ IF EXISTS ] name"""

def get_sql(procedure, name):
    res = {'sql': ''}
    if procedure == 'insert' and name == 'table':
        res['sql'] = get_table_insert()
    elif procedure == 'modify' and name == 'table':
        res['sql'] = get_table_modify()
    elif procedure == 'delete' and name == 'table':
        res['sql'] = get_table_delete()
    elif procedure == 'insert' and name == 'function':
        res['sql'] = get_function_insert()
    elif procedure == 'modify' and name == 'function':
        res['sql'] = get_function_modify()
    elif procedure == 'delete' and name == 'function':
        res['sql'] = get_function_delete()
    elif procedure == 'insert' and name == 'trigger':
        res['sql'] = get_trigger_insert()
    elif procedure == 'modify' and name == 'trigger':
        res['sql'] = get_trigger_modify()
    elif procedure == 'delete' and name == 'trigger':
        res['sql'] = get_trigger_delete()
    elif procedure == 'insert' and name == 'view':
        res['sql'] = get_view_insert()
    elif procedure == 'modify' and name == 'view':
        res['sql'] = get_view_modify()
    elif procedure == 'delete' and name == 'view':
        res['sql'] = get_view_delete()
    elif procedure == 'insert' and name == 'procedure':
        res['sql'] = get_procedure_insert()
    elif procedure == 'modify' and name == 'procedure':
        res['sql'] = get_procedure_modify()
    elif procedure == 'delete' and name == 'procedure':
        res['sql'] = get_procedure_delete()
    elif procedure == 'insert' and name == 'index':
        res['sql'] = get_index_insert()
    elif procedure == 'modify' and name == 'index':
        res['sql'] = get_index_modify()
    elif procedure == 'delete' and name == 'index':
        res['sql'] = get_index_delete()
    elif procedure == 'insert' and name == 'schema':
        res['sql'] = get_schema_insert()
    elif procedure == 'modify' and name == 'schema':
        res['sql'] = get_schema_modify()
    elif procedure == 'delete' and name == 'schema':
        res['sql'] = get_schema_delete()
    return res

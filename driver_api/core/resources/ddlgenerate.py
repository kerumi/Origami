import json, requests
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
parser = reqparse.RequestParser()
parser.add_argument('hostname')
parser.add_argument('port')
parser.add_argument('password')
parser.add_argument('username')
parser.add_argument('catalog')
parser.add_argument('db_password')
parser.add_argument('db_user')
parser.add_argument('object_type')
parser.add_argument('object_name')
parser.add_argument('schema')
parser.add_argument('table')
parser.add_argument('tableNamePattern')

class DDLGen(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            payload = {'broker_hostname': args['hostname'], 'broker_port': args['port'], 'password': args['password'], 'username': args['username']}
            r = requests.post('http://localhost:8888/rest/app/signon', data=payload)
            dbdata = {'catalog': args['catalog'], 'password': args['db_password'], 'user': args['db_user']}
            db_req = requests.post('http://localhost:8888/db/dbLogin', data=dbdata, cookies=r.cookies, auth=(args['username'], args['password']))
            resp = json.loads(db_req.text)
            print r.cookies
            db_id = resp['databaseID']
            ddldata = {'catalog': args['catalog'], 'context_type': args['object_type'], 'context_value': args['object_name'], 'databaseID': db_id, 'schema': args['schema'], 'schemaPattern': args['schema'], 'table': args['table'],
            'tableNamePattern': args['tableNamePattern']}
            ddl_req = requests.post('http://localhost:8888/db/generateScript', data=ddldata, cookies=r.cookies, auth=(args['username'], args['password']))
        except Exception as e:
            error = "An error ocurred: "+str(e)
            return error, 500
        return json.loads(ddl_req.text)

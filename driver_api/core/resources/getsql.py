import json, requests
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
import core.resources.sqltemplates as templates
parser = reqparse.RequestParser()
parser.add_argument('type')
parser.add_argument('procedure')

class GetSQL(Resource):
    def post(self):
        args = parser.parse_args()
        return templates.get_sql(args['procedure'], args['type'])

import json, requests
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
parser = reqparse.RequestParser()
parser.add_argument('hostname')
parser.add_argument('port')
parser.add_argument('password')
parser.add_argument('username')
parser.add_argument('catalog')
parser.add_argument('db_password')
parser.add_argument('db_user')
parser.add_argument('schema')
parser.add_argument('sql')

class SQLStatement(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            payload = {'broker_hostname': args['hostname'], 'broker_port': args['port'], 'password': args['password'], 'username': args['username']}
            r = requests.post('http://localhost:8888/rest/app/signon', data=payload)
            dbdata = {'catalog': args['catalog'], 'password': args['db_password'], 'user': args['db_user']}
            db_req = requests.post('http://localhost:8888/db/dbLogin', data=dbdata, cookies=r.cookies, auth=(args['username'], args['password']))
            resp = json.loads(db_req.text)
            print r.cookies
            db_id = resp['databaseID']
            sqldata = {'catalog': args['catalog'], 'schema': args['schema'], 'schemaPattern': args['schema'], 'operation': 'runSQL', 'limit': 100, 'databaseID': db_id, 'start': 0, 'sql': args['sql']}
            ddl_req = requests.post('http://localhost:8888/db/runSQL', data=sqldata, cookies=r.cookies, auth=(args['username'], args['password']))
        except Exception as e:
            error = "An error ocurred: "+str(e)
            return error, 500
        return json.loads(ddl_req.text)

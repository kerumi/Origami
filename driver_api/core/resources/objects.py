import json
from flask import Flask, session
from flask_restful import reqparse, abort, Api, Resource
from core.nuodb.dbmanager.query import Query
parser = reqparse.RequestParser()
parser.add_argument('database')
parser.add_argument('user')
parser.add_argument('password')


class Schemas(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SCHEMAS;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

class Tables(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.TABLES;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

class Indexes(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.INDEXES;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

class Views(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.VIEW_TABLES;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

class Functions(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.FUNCTIONS;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

class Procedures(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.PROCEDURES;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result


class Triggers(Resource):
    def post(self):
        try:
            args = parser.parse_args()
            credentials = {'database': args['database'], 'host': "localhost", 'user': args['user'], 'password': args['password'], 'options': {"schema": "SYSTEM"}}
            query = Query(credentials)
            result = query.execute("SELECT * FROM SYSTEM.TRIGGERS;")
        except Exception as e:
            error = "An error ocurred on the query: "+str(e)
            return error, 500
        return result

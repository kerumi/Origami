export default {
  currentUser(state){
    return state.user
  },
  isLoggedIn: state => {
    return state.isLoggedIn
  },
  queryTable: state => {
    return state.queryResultTable;
  },
  queryTableCols: state => {
    return state.queryResultTable.cols
  },
  queryTableRows: state => {
    return state.queryResultTable.rows
  },
  currentDB: state => {
    return state.currentDatabase
  },
  currentSchema: state => {
    return state.currentSchema
  },
  currentConnectionCreds: state => {
    return {
      db_user: state.db_user,
      db_password: state.db_password
    }
  },
  showDbPrompt: state => {
    return state.showDbConnectPrompt
  },
  databases: state => {
    return state.databases
  },
  schemas : state => {
    return state.schemas.rows
  },
  hierarchyTree: state => {
    return state.hierarchyTree
  },
  currentQuery: state => {
    return state.currentQuery
  }
}

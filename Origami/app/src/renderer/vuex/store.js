import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import getters from './getters'
import mutations from './mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      name: '',
      password : ''
    },
    isLoggedIn: !!window.localStorage.getItem('user'),
    queryResultTable: {
      cols: [],
      rows: []
    },
    currentSchema: window.localStorage.getItem('schema'),
    currentDatabase: window.localStorage.getItem('db'),
    db_user: window.localStorage.getItem('db_user'),
    db_password: window.localStorage.getItem('db_password'),
    showDbConnectPrompt: !!window.localStorage.getItem('db-prompt'),
    databases: [],
    schemas: [],
    hierarchyTree: [],
    currentQuery: ''
  },
  actions,
  getters,
  mutations,
  strict: process.env.NODE_ENV !== 'production'
})
